# [CPac](https://gitlab.com/ACPPR/CPac) - [Python](https://www.python.org/) #

Forks [python](https://www.python.org/) for the [CPac](https://gitlab.com/ACPPR/CPac) system.

# [Python 2.7.18](https://www.python.org/downloads/release/python-2718/) #

This is Python version 2.7.18
=============================

Copyright (c) 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011,
2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020 Python Software Foundation.  All
rights reserved.

Copyright (c) 2000 BeOpen.com.
All rights reserved.

Copyright (c) 1995-2001 Corporation for National Research Initiatives.
All rights reserved.

Copyright (c) 1991-1995 Stichting Mathematisch Centrum.
All rights reserved.


License information
-------------------

See the file "LICENSE" for information on the history of this
software, terms & conditions for usage, and a DISCLAIMER OF ALL
WARRANTIES.

This Python distribution contains no GNU General Public Licensed
(GPLed) code so it may be used in proprietary projects just like prior
Python distributions.  There are interfaces to some GNU code but these
are entirely optional.

All trademarks referenced herein are property of their respective
holders.

------------------

--Guido van Rossum (home page: http://www.python.org/~guido/)